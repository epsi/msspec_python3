############
Contributors
############


MsSpec has benefited from the help of many people. Its core results from the long-term
collaboration of D. Sébilleau (Université de Rennes-1, France) with C. R. Natoli (LNFINFN,
Italy). More specifically, cluster gen.f and the various versions of spec.f have been
written by D. Sébilleau1 while phagen scf.f is due to C. R. Natoli. Other contributions have
been made possible through several joint CNRS/Chinese Academy of Sciences projects with Z.-
Y. Wu's group at the Beijing Synchrotron Radiation Facility and at the National Synchrotron
Radiation Laboratory in Hefei (China). MsSpec has also benefited from the post-doctoral grant
awarded to K. Hatada by the Région Bretagne, and from the four year LighTnet European
network. Currently, MsSpec is part of the FP7-funded European network MSNano and of the
COST-funded European action EUSpec.

The other contributors are:

*	D. Agliz (Université Ibnou-Zohr, Agadir, Morocco): contribution to the 
	implementation of the Rehr-Albers method
*	M. Gavaza (City University, Hong Kong): symmetrized form of the multiple 
	scattering series
*	F. Da Pieve (Università di Roma 3): implementation of the Auger case into 
	phagen scf.f and contribution to the implementation of the APECS cross-section
*	K. Hatada (LNF-INFN, Italy): help to write up the makefile that generates the 
	spec.f code and implementation of the parallel version. Implementation of the 
	non muffin-tin routines into phagen scf.f and into spec.f.
*	H.F. Zhao (BSRF, Beijing, China and Université de Rennes-1, France): correlation 
	expansion.
*	J.-Y. Wang (BSRF, Beijing, China): Graphical User Interface.
*	E.-R. Li (BSRF, Beijing, China): Graphical User Interface.
*	L. Frein (Université de Rennes-1, France): artwork for the Graphical User 
	Interface.
*	A. Carré (Université de Rennes-1, France): 
	previous Website
*	P. Le Meur (Université de Rennes-1, France): 
	Python interface 
*	S. Tricot (Université de Rennes-1, France): Python interface, website, 
	online tutorials and documentation
*	P. Schieffer (Université de Rennes-1, France): XPD related 
	improvements

The package has also benefited from many users and colleagues who pointed out bugs or
suggested improvements or new features. Among them, S. Ababou-Girard, B. Lépine, Y. Lu,
T. Jaouen and P. Schieffer (Université de Rennes-1, France), F. Scheurer (Université Louis
Pasteur, Strasbourg, France), P. Wetzel (Université de Haute-Alsace, Mulhouse, France), R.
Belkhou (SOLEIL, France), M. Zanouni (Université de Tanger, Maroc), A. Souissi (Université
de Bizerte, Tunisie), R. Gunnella (Università di Camerino, Italy) and J. Osterwalder (Université
de Zürich, Switzerland).
The empty sphere features have been tested with the help of K. Kuroda and S. Tadano
(Chiba university, Japan).

Work on the next version (version 2.0) is currently under development by:

*	D. Sébilleau (Université de Rennes-1, France): 
	lead development
*	C. R. Natoli (LNF-INFN, Italy): new features in phagen scf.f (scalar-relativistic, impact
	parameter representation, EELS and (e,2e) radial integrals, ...) and interface with
	VASP electronic structure code
*	K. Hatada (Université de Rennes-1, France): BEEM spectroscopy, interface with VASP
	electronic structure code
*	H.-F. Zhao (BSRF, Beijing, China): REXS 
	spectroscopy
*	J.-Q. Xu (NSRL, Hefei, China): EELS spectroscopy, interface with VASP electronic
	structure code
*	W.-S. Chu (NSRL, Hefei, China): portage to Windows operating system 
	(through Cygwin)
*	R. Choubisa (BITS, Pilani, India): (e,2e) 
	spectroscopy
*	P. Krüger (Chiba university, Japan): interface with VASP electronic 
	structure code
*	A. Calvez (Université de Rennes-1, France): 
	optimization loop
*	T. Balège and L. Strafella (Université de Rennes-1, France): 
	portage to Fortran 90
*	S. Tricot (Université de Rennes-1, France): Python interface, website, online 
	tutorials and documentation, graphical user interface
*	P. Schieffer (Université de Rennes-1, France): XPD related 
	improvements
*	G. Raffy ( Université de Rennes-1, France): Graphical user 
	interface
