.. _allparameters:

#############################
All the simulation parameters
#############################


.. contents:: Contents
    :depth: 2
    :local:

Common parameters
*****************

.. include:: spectroscopies/common_parameters.inc

Spectroscopy parameters
***********************


.. include:: spectroscopies/ped/ped_parameters.inc

.. include:: spectroscopies/eig/eig_parameters.inc