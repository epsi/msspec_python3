##############
Spectroscopies
##############



.. toctree::
    :maxdepth: 1

    ped/ped
    aed/aed
    apecs/apecs
    exafs/exafs
    leed/leed
    eig/eig




