.. warning::

    For the moment, this spectroscopy mode is not available with the Python interface.

    You have to run the classical version of MsSpec to perform such a calculation.
    Please see ??? for the details on how to run MsSpec like this.