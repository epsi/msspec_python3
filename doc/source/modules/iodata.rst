:orphan:

.. automodule:: iodata
    :members: Data, DataSet, _DataSetView
    :undoc-members:
    :show-inheritance: