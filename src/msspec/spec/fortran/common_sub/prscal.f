C
C=======================================================================
C
      FUNCTION PRSCAL(A1,A2)
C
C  This function computes the dot product of the two vectors A1 and A2
C
      DIMENSION A1(3),A2(3)
C
      PRSCAL=A1(1)*A2(1)+A1(2)*A2(2)+A1(3)*A2(3)
C
      RETURN
C
      END
