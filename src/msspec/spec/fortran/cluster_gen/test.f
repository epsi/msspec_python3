C
C=======================================================================
C
      SUBROUTINE TEST(NIV,ROT,NATYP,NBZ,NAT2,ISURF,COUP,*)
C
      USE DIM_MOD
C
      DIMENSION ATOME1(3,NATCLU_M),COORD1(3,NATCLU_M)
      DIMENSION IRE1(NATCLU_M,2),NATYP(NATM)
      DIMENSION NATYP1(NATM),VALZ1(NATCLU_M),ROT(3,3)
C
      NMAX1=(2*NIV+3)**3
      NV1=NIV+1
      CALL AMAS(NV1,ATOME1,COORD1,VALZ1,ISURF,COUP,ROT,IRE1,NATYP1,NBZ,N
     &AT2,NCOUCH,NMAX1)
      DO 10 I=1,NAT2
        IF(NATYP(I).NE.NATYP1(I)) RETURN 1
  10  CONTINUE
C
      RETURN
C
      END
