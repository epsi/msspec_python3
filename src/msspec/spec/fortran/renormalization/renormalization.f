      SUBROUTINE COEF_RENORM(NDIF)
C
C  This subroutine computes the coefficients for the renormalization
C     of the multiple scattering series. These coefficients are 
C     expressed as C_REN(K) where K is the multiple scattering order.
C     REN2 is the value of the mixing (or renormalization) parameter.
C
C     NDIF is the scattering order at which the series is truncated, 
C     so that K varies from 0 to NDIF.
C
C  COMMON /RENORM/:
C
C     I_REN = 1 : renormalization in terms of G_n matrices (n : N_REN)
C           = 2 : renormalization in terms of the Sigma_n matrices
C           = 3 : renormalization in terms of the Z_n matrices
C           = 4 : renormalization in terms of the Pi_1 matrix
C
C     N_REN = n
C     
C     REN   = REN_R+IC*REN_I : omega
C
C                                        Last modified : 11 Apr 2019
C
      USE DIM_MOD
      USE C_RENORM_MOD
      USE RENORM_MOD
C
      REAL C(0:NDIF_M,0:NDIF_M)
C
      COMPLEX REN,REN2,COEF1,COEF2,ZEROC,ONEC,IC
      COMPLEX Y1(0:NDIF_M,0:NDIF_M)
C
C
      ZEROC=(0.,0.)
      ONEC=(1.,0.)
      IC=(0.,1.)
C
      REN=REN_R+IC*REN_I                                         ! omega
C
C  Initialisation of renormalization coefficients
C
      DO J=0,NDIF
        C_REN(J)=ZEROC
      ENDDO
C                                               
C  Computing the binomial coefficients C(N,K) = (N) = N! / K! (N-K)!
C                                               (K)
CCCC  2019.06.09 Aika
      c=0.0
CCCC  2019.06.09 Aika
      C(0,0)=1.
      C(1,0)=1.
      C(1,1)=1.
      DO N=2,NDIF
        C(N,0)=1.
        C(N,N)=1.
        DO K=1,N-1
          C(N,K)=C(N-1,K)+C(N-1,K-1)
        ENDDO
      ENDDO
C
      IF(I_REN.LE.3) THEN
C
C  Computing the modified renormalization parameter REN2 (g_n,s_n,zeta_n)
C
        IF(I_REN.EQ.1) THEN
C
C.....(g_n,G_n) renormalization
C
          REN2=REN**N_REN                                        ! g_n = omega^n
C
        ELSEIF(I_REN.EQ.2) THEN
C
C.....(s_{n},Sigma_n) renormalization
C
          REN2=(ONEC-REN**(N_REN+1))/(FLOAT(N_REN+1)*(ONEC-REN)) ! s_n
C
        ELSEIF(I_REN.EQ.3) THEN
C
C.....(zeta_{n},Z_n) renormalization
C
C     2019.04.29
C         REN2=(REN-ONEC)**(N_REN+1)                             ! zeta_n  
C     2019.06.09
C          REN2=-(REN-ONEC)**(N_REN+1)                             ! zeta_n  
           REN2=-(ONCE-REN)**(N_REN+1)                              ! zeta_n  
C
        ENDIF
C
C     AT & MTD 2019.04.17 - summation over j ?
        DO K=0,NDIF
           c_ren(k)=zeroc
           DO J=K,NDIF
              C_REN(K)=C_REN(K)+c(j,k)*(ONEC-REN2)**(J-K)
           ENDDO
           c_ren(k)=c_ren(k)*ren2**(k+1)
        ENDDO
C        
C        DO K=0,NDIF
C          COEF1=REN2**(K+1)
C          DO J=K,NDIF
C            COEF2=(ONEC-REN2)**(J-K)
C            C_REN(J)=C_REN(J)+COEF1*COEF2*C(J,K)
C          ENDDO
C        ENDDO
C
      ELSEIF(I_REN.EQ.4) THEN
C
C     Loewdin (Pi_1) renormalization for n = 1
C
C     Notation: Y1(M,K) : [Y_1^m]_k
C     2019.06.09    
C     Notation: Y1(K,M) : [Y_1^k]_m         
C
        COEF1=ONEC-REN                                           ! (1 - omega)
        DO K=0,NDIF
          Y1(K,K)=COEF1**K
          IF(K.LT.NDIF) THEN
            DO M=K+1,NDIF
               COEF2=(REN**(M-K))*(COEF1**(2*K-M))
C 2019.04.19 AT & MTD
C            Y1(M,K)=COEF2*(C(K,M-K)+COEF1*C(K,M-K-1))
              Y1(K,M)=COEF2*(C(K,M-K)+COEF1*C(K,M-K-1))
            ENDDO
          ENDIF
        ENDDO
C
        DO K=0,NDIF
          IN=INT(K/2)
          C_REN(K)=ZEROC
          DO M=IN,K
            C_REN(K)=C_REN(K)+Y1(M,K)
          ENDDO
        ENDDO
C
      ENDIF
C
      END
C
C=======================================================================
C
      SUBROUTINE COEF_LOEWDIN(NDIF)
C
C  This subroutine computes the coefficients for the Loewdin expansion
C     of the multiple scattering series. These coefficients are 
C     expressed as C_LOW(K) where K is the multiple scattering order.
C     REN is the value of the mixing (or renormalization) parameter.
C     NDIF is the scattering order at which the series is truncated, 
C     so that K varies from 0 to NDIF.
C
C  Corresponds to parameter I_REN = 5
C
C  Notation: X(K,N) = X_n(omega,k)
C
C
C                                        Last modified : 11 Apr 2019
C
      USE DIM_MOD
      USE C_RENORM_MOD, C_LOW => C_REN
      USE RENORM_MOD
C
      COMPLEX X(0:NDIF_M,0:NDIF_M),SUM_L,POWER
      COMPLEX REN,ZEROC,ONEC,IC
C
C
      ZEROC=(0.,0.)
      ONEC=(1.,0.)
      IC=(0.,1.)
C
      REN=REN_R+IC*REN_I                                         ! omega
C
C  Initialisation of renormalization coefficients
C
      DO J=0,NDIF
        C_LOW(J)=ZEROC
      ENDDO
C
C  Computing the X(N,K) coefficients, with K <= N
C
      POWER=ONEC/REN
      DO N=0,NDIF
        POWER=POWER*REN                                          ! omega^n
        IF(N.EQ.0) THEN 
          X(N,0)=ONEC
        ELSE
          X(N,0)=ZEROC
        ENDIF
        DO K=1,NDIF
          IF(K.GT.N) THEN
            X(N,K)=ZEROC
          ELSEIF(K.EQ.N) THEN
            X(N,K)=POWER*X(N-1,K-1)
          ELSE
            X(N,K)=X(N-1,K)*(REN-POWER) + POWER*X(N-1,K-1)
          ENDIF
        ENDDO
      ENDDO
C
C  Calculation of L_n(omega,NDIF)
C
      DO N=0,NDIF
        SUM_L=ZEROC
        DO K=N,NDIF
          SUM_L=SUM_L+X(K,N)
        ENDDO
        C_LOW(N)=SUM_L
      ENDDO

      END

